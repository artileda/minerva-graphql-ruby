# Minerva (Back-End)

### EN

Minerva is a text-based MOOC build with Ruby (Sinatra),
Javascript (Svelte),Postrgresql and GraphQL. this just backbone service
for providing front-end queries.

- Philosopy

Name taken from Roman mitology : Minerva, goddess of wisdom , knowledge, war and art.

### ID

Minerva merupakan Kelas Daring Masif Terbuka (KDMT) berbasis
teks yang dibangun mengunakan Ruby (Sinatra) , Javascript 
Svelte) ,Postgresql  dan GraphQL. Ini merupakan layanan belakang untuk
menyediakan data antarmuka depan.

- Filosofi

Nama Minerva diambil dari Dewi ilmu pengetahuan , kebijaksanaan , perang
 dan seni mitologi Romawi.

## Installation / Installasi

Tested on MX Linux

make sure to adjust database configuration @ `config/database.yml`
and make your database authorization granting to create and
delete database.

```bash

sudo apt install ruby postgresql libpq-dev
bundle install
bundle db:drop db:create db:migrate db:seed
ruby minerva.rb

```
Port start on 4567.

> Tutorial
- [Install Postgress Gem](https://wikimatze.de/installing-postgresql-gem-under-ubuntu-and-mac/)

## Usage

### To adding material to kelas

Currently not build mutation query, this will be add soon. directly add to seed db @ `db/seeds.rb` then `bundle rake db:seed`.



### To run the service
- Start the Minerva
- Use Postman or other GraphQL Client
- Post query to address:4567/graphql

## Minerva Goals

free open source MOOC platform for providing sharing knowledge medium and give a structured learning path. 

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Community

Dicussion Group on :
- Telegram
  - Minerva Project
    - [EN](https://t.me/joinchat/LMEzUxOd5W9xTxtO8nJnVw)
    - [ID](https://t.me/joinchat/LMEzUxdG8nef5v2N2kX5bA)

## License
[MIT](https://gitlab.com/nagiten/minerva-graphql-ruby/blob/master/LICENSE)
