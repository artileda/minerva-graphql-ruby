require 'graphql'
require_relative 'graph'

class Kelas < GraphObject
  field :id , ID , null: false
  field :nama, String, null: false
  field :deskripsi, String , null: false
end 
