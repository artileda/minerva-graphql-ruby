require 'graphql'
require_relative 'graph'

class Materi < GraphObject
  field :id, ID , null: false
  field :nama,String,null: false
  field :konten,String,null: false
  field :qid,ID,null: false
  field :kela_id,ID,null: false
end
