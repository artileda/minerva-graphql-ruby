require 'graphql'
require_relative 'graph_mutation'

class CreateMateri < GraphMutation
  argument :nama, String , required: true
  argument :konten, String , required: true
  argument :kelas, ID , required: true

  field :success, Boolean , null: true
  field :msg, [String], null: true

  def resolve(nama:,konten:,kelas:)
    materi = Materies.new(
      nama: nama,
      konten: konten,
      kela_id: kelas
    )

    if materi.save
      {
        success: true,
        msg: []
      }
    else
      {
        success: false,
        msg: materi.errors.full_messages
      }
    end
  end

end

class UpdateMateri < GraphMutation
  argument :id, ID , required: true
  argument :nama, String , required: false
  argument :konten, String , required: false
  argument :kelas, ID , required: false

  field :success, Boolean , null: true
  field :msg, [String], null: true

  def resolve(id:,nama:,konten:,kelas:)
    materi = Materies.find(id)
    materi.nama = nama unless nama.nil? || nama.empty
    materi.konten = konten unless konten.nil? || nama.empty?
    materi.kela_id = kelas unless kelas.nil? || nama.empty?

    if materi.save
      {
        success: true,
        msg: []
      }
    else
      {
        success: false,
        msg: materi.errors.full_messages
      }
    end
  end

end

class DestroyMateri < GraphMutation
  argument :id, ID,required: true

  field :success, Boolean, null: true
  field :msg, [String], null: true

  def resolve(id:)
    materi = Materies.find(id)

    if materi.destroy
      {
        success: true,
        msg: []
      }
    else
      {
        success: false,
        msg: materi.errors.full_messages
      }
    end
  end

end
