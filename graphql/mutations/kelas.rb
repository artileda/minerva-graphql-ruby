require 'graphql'
require_relative 'graph_mutation'


class CreateKelas < GraphMutation
  argument :nama, String , required: true
  argument :deskripsi, String , required: true

  field :success, Boolean , null: true
  field :msg, [String], null: true

  def resolve(nama:,deskripsi:)
    kelas = Kelas.new(
      nama: nama,
      deskripsi: deskripsi
    )

    if kelas.save
      {
        success: true,
        msg: []
      }
    else
      {
        success: false,
        msg: kelas.errors.full_messages
      }
    end
  end

end

class UpdateKelas < GraphMutation
  argument :id, ID , required: true
  argument :nama, String , required: false
  argument :deskripsi, String , required: false

  field :success, Boolean , null: true
  field :msg, [String], null: true

  def resolve(id:,nama:,konten:,kelas:)
    kelas = Kelas.find(id)
    kelas.nama = nama unless nama.nil?
    kelas.deskripsi = deskripsi unless deskripsi .nil?

    if kelas.save
      {
        success: true,
        msg: []
      }
    else
      {
        success: false,
        msg: kelas.errors.full_messages
      }
    end
  end

end

class DestroyKelas < GraphMutation
  argument :id, ID,required: true

  field :success, Boolean, null: true
  field :msg, [String], null: true

  def resolve(id:)
    kelas = Kelas.find(id)

    if kelas.destroy
    {
      success: true,
      msg: []
    }
    else
    {
      success: false,
      msg: kelas.errors.full_messages
    }
    end
  end
end
