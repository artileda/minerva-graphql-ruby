require 'graphql'
require_relative 'types/kelas'
require_relative 'types/materi'

class Query < GraphQL::Schema::Object
  description "Root Schema"

  field :kelas,[Kelas],null: true do
    argument :nama, String ,required: false
  end

  field :materi,[Materi],null: true do
    argument :queue,String,required: false, default_value: ""
    argument :kelas,String,required: false,default_value: ""
  end

  def kelas(nama:)
    unless nama.nil? || nama.eql?("")
      [Kela.find_by(nama: nama)]
    else
      Kela.all
    end
  end

  def materi(kelas:,queue:)

    unless queue.eql?("")
      query = Materies.where(qid: queue,kela_id: kelas.to_i)
        .order(:qid)
      puts query
      if query.empty?
        []
      else
        query.map{|materi| materi}
      end
    else
      if kelas != ""
        Materies.where(kela_id: kelas.to_i)
          .order(:qid)
          .map{|materi| materi}
      else
        []
      end
    end

  end

end
