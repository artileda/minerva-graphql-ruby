require 'graphql'

require_relative 'mutations/kelas'
require_relative 'mutations/materi'

class Mutations < GraphQL::Schema::Object

  field :createKelas, mutation: CreateKelas
  field :updateKelas, mutation: UpdateKelas
  field :destroyKelas, mutation: DestroyKelas

  field :createMateri, mutation: CreateMateri
  field :updateMateri, mutation: UpdateMateri
  field :destroyMateri, mutation: DestroyMateri

end
