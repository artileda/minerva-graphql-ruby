require 'graphql'

require_relative 'query'
require_relative 'mutation'

class GraphSchema < GraphQL::Schema
  use GraphQL::Execution::Errors

  query Query
  mutation Mutations

  rescue_from(NoMethodError) do |err,obj,args,ctx,field|
    raise GraphQL::ExecutionError, "Error : #{err}"
  end

end
