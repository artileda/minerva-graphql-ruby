require 'sinatra'
require 'sinatra/activerecord'
require 'sinatra/json'
require 'sinatra/cross_origin'
require 'rack/contrib'
require "sinatra/reloader" if development?

require_relative 'graphql/schema'
require_relative 'models/kela'
require_relative 'models/materies'


set :database_file, "config/database.yml"
use Rack::PostBodyContentTypeParser 
  
configure :development do
  register Sinatra::Reloader
end

configure do
  enable :cross_origin
end

before do
  response.headers['Access-Control-Allow-Origin'] = '*'
end
 
options "*" do
  response.headers["Allow"] = "GET, PUT, POST, DELETE, OPTIONS"
  response.headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type, Accept, X-User-Email, X-Auth-Token"
  response.headers["Access-Control-Allow-Origin"] = "*"
  
end

get '/' do
  @kelas = Kela.all
  json @kelas
end

post '/graphql' do
  @res = GraphSchema.execute(
    params[:query],
    variables: params[:variables]
  )

  json @res

end


