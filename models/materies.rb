class Materies < ActiveRecord::Base
  belongs_to :kela
  before_save :queue

  def queue
    list = Materies.where(kela_id: self.kela_id)
    if list.length.eql?(0)
      self.qid = 0
    elsif self.qid.nil?
      self.qid = list.last.qid + 1
    end
  end
end
