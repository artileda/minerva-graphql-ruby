class CreateKelas < ActiveRecord::Migration[6.0]
  def change
    create_table :kelas do |t|
      t.string :nama
      t.string :deskripsi
    end
  end
end
