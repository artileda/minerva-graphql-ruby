class CreateMateri < ActiveRecord::Migration[6.0]
  def change
    create_table :materies do |t|
      t.references :kela
      t.integer :qid
      t.string :nama
      t.text :konten
    end
  end
end
